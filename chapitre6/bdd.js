/*
 * bdd.js
 * Description: Contient les fonctions en rapport avec la base de données
 * Auteur: Victor Kabdebon
 * Site: http://www.victorkabdebon.com
 * */

var mongo = require("mongoskin");

function retourneCollection() {
	return mongo.db("localhost:27017/MonTODO", {'safe': false}).collection('Taches');
}


function sauvegarderTache(tache, callback) {
	retourneCollection().save(tache,
			{'upsert': true},
			function (erreur) {
			callback(erreur);
		});
}

function chargerTache(tacheId, callback) {
	var collection = retourneCollection();
	collection.findOne({'_id': collection.id(tacheId)}, function (erreur, tache) {
		callback(erreur, tache);
	});
}

function chargerListeTaches(callback) {
	var collection = retourneCollection();
	collection.find({'fait': false}).toArray(function (erreurs, taches) {
		callback(erreurs, taches);
	});
}

exports.sauvegarderTache = sauvegarderTache;
exports.chargerTache = chargerTache;
exports.chargerListeTaches = chargerListeTaches;


