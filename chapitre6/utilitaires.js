/* 
 * utilitaires.js
 * Description: Une serie d'outils et fonctions pour simplifier les opérations ordinaires.
 * Auteur: Victor Kabdebon
 * Site Web: http://www.victorkabdebon.com
 * */

var url = require("url");
var mu = require("mu2");

var cheminVersTemplates = "./pages/";

function genereChemin(nomTemplate) {
	return cheminVersTemplates + nomTemplate + ".mustache";
}

function extraitChemin(requete) {
	var chemin  = url.parse(requete.url).path;
	if (chemin === "/" || chemin === "") {
		return "/";
	}
	var elements = chemin.split("/");
	if (elements.length > 1) {
		return elements[1];
	}
	return "/";
}

function extraitParametre(requete) {
	var chemin  = url.parse(requete.url).path;
	var elements = chemin.split("/");
	if (elements.length > 2) {
		return elements[2];
	}
	return null;
}

function writeHead(reponse, code) {
	reponse.writeHead(code, {"Content-type": "text/html"});
}

function voir404(reponse) {
	writeHead(reponse, 404);
	var stream = mu.compileAndRender(genereChemin("404"), {});
	stream.pipe(reponse);
}

exports.genereChemin = genereChemin;
exports.extraitChemin = extraitChemin;
exports.extraitParametre = extraitParametre;
exports.voir404 = voir404;
