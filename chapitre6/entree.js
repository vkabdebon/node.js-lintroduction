/*
 * entree.js
 * Description: Point d'entrée dans le programme.
 * Auteur: Victor Kabdebon
 * Site Web: http//www.victorkabdebon.com
 * */


var serveur = require("./serveur");
var requete = require("./processeurDeRequete");

var processeur = {};

processeur.ecrire = requete.ecrireNouvelleTache;
processeur.tache = requete.voirTache;
processeur.liste = requete.voirTachesAFaire;
processeur.supprimer = requete.supprimerTache;
processeur["/"] = requete.voirAccueil;

serveur.demarre(processeur);

