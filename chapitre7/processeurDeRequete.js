/*
 * processeurDeRequete.js
 * Description: -
 * Auteur: Victor Kabdebon
 * Site: http://www.victorkabdebon.com
 * */

var bdd = require("./bdd");
var utilitaires = require("./utilitaires");

var mu = require("mu2");
var formidable = require("formidable");

function creeTache(champs) {
	var tache = {};
	tache.titre = champs.titre;
	tache.description = champs.description;
	tache.fait = false;
	return tache;
}

function ecrireNouvelleTache(requete, reponse) {
	if (requete.method === "GET") {
		var stream = mu.compileAndRender(utilitaires.genereChemin("creertache"), {});
		stream.pipe(reponse);
	} else {
		var formulaire = new formidable.IncomingForm();
		formulaire.parse(requete, function (erreurs, champs, fichiers) {
			var tache = creeTache(champs);
			bdd.sauvegarderTache(tache, function (erreur) {
				var stream = mu.compileAndRender(utilitaires.genereChemin("succes"), {});
				stream.pipe(reponse);
			});
		});
	}
}


function voirTachesAFaire(requete, reponse) {
	bdd.chargerListeTaches(function (erreurs, taches) {
		var stream = mu.compileAndRender(
			utilitaires.genereChemin("voirlistetaches"),
			{'taches': taches}
		);
		stream.pipe(reponse);
	});
}

function voirTache(requete, reponse) {
	var tacheId = utilitaires.extraitParametre(requete);
	bdd.chargerTache(tacheId, function (erreur, tache) {
		var stream = mu.compileAndRender(
			utilitaires.genereChemin("voirtache"),
			{'tache': tache}
		);
		stream.pipe(reponse);
	});
}

function supprimerTache(requete, reponse) {
	var tacheId = utilitaires.extraitParametre(requete);
	bdd.chargerTache(tacheId, function (erreur, tache) {
		tache.fait = true;
		bdd.sauvegarderTache(tache, function (erreur) {
			var stream = mu.compileAndRender(utilitaires.genereChemin("succes"), {});
			stream.pipe(reponse);
		});
	});
}

function voirAccueil(requete, reponse) {
	var stream = mu.compileAndRender(
			utilitaires.genereChemin("accueil"),
			{}
		);
	stream.pipe(reponse);
}

exports.ecrireNouvelleTache = ecrireNouvelleTache;
exports.voirTachesAFaire = voirTachesAFaire;
exports.supprimerTache = supprimerTache;
exports.voirTache = voirTache;
exports.voirAccueil = voirAccueil;

