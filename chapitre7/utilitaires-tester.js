/**
 * simpleTODOTester.js
 * Description: Des tests simples pour utilitaires.js
 * Auteur: Victor Kabdebon
 * Site Web: http://www.victorkabdebon.com
 * */

var assert = require("assert");
var http = require("http");

var utilitaires = require("./utilitaires");

var chemintest = utilitaires.genereChemin("test");
assert.equal(chemintest, "./pages/test.mustache");

