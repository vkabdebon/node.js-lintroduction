/*global describe:false, it:false */

/*
 * Description: Des tests, des vrais avec mocha!
 * Auteur: Victor Kabdebon
 * */


var utilitaires = require("../utilitaires");
var should = require("should");


describe("utilitaire", function () {
	describe("genereChemin", function () {
		it("devrait générer un chemin de la forme ./pages/template.mustache", function () {
			utilitaires.genereChemin("test").should.equal("./pages/test.mustache");
		});
	});
});
