/*global describe:false, it:false, before:false, after:false */

/* 
 * pages-tester.js 
 * Description: Ces tests verifient les pages
 * Auteur: Victor Kabdebon
 * License: -
 * */


var http = require("http");
var util = require("util");

var adresseServeur = "http://localhost:8888";
var fork = require("child_process").fork;


describe("pages", function () {

	var processus;

	before(function (done) {
		processus = fork("entree.js", null, null);
		processus.on("message", function (msg) {
			if (msg === "OK") {
				done();
			}
		});
	});

	after(function () {
		processus.kill();
	});


	describe("du site", function () {
		it("/ doit renvoyer un code 200", function (done) {
			http.get(adresseServeur + "/", function (reponse) {
				reponse.statusCode.should.equal(200);
				done();
			});
		});
		it("/ doit renvoyer un code 200", function (done) {
			http.get(adresseServeur + "/ecrire/", function (reponse) {
				reponse.statusCode.should.equal(200);
				done();
			});
		});
	});

	describe("d'erreur", function () {
		it("doit renvoyer un code 404", function (done) {
			http.get(adresseServeur + "/abcdef/", function (reponse) {
				reponse.statusCode.should.equal(404);
				done();
			});
		});
	});
});


