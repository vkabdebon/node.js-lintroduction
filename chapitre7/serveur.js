/*
 * serveur.js
 * Description: Code pour le serveur
 * Auteur: Victor Kabdebon
 * Site Web: http://www.victorkabdebon.com
 * */
var http = require("http");
var utilitaires = require("./utilitaires");

function demarre(processeurRequete) {
	var serveur = http.createServer(function (requete, reponse) {
		var chemin = utilitaires.extraitChemin(requete);
		if (typeof (processeurRequete[chemin]) === 'function') {
			processeurRequete[chemin](requete, reponse);
		} else {
			utilitaires.voir404(reponse);
		}
	});
	serveur.listen(8888, function(){
		if(process.send){
			process.send("OK");
		}
	});
}

exports.demarre = demarre;
