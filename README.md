Source code pour Node.js L'introduction
#######################################

Author: Victor Kabdebon

Code source accompagnant le livre 'Node.js, L'introduction'.
Le code source est organisé par chapitre ( pour le chapitre X, le chemin est ./chapitreX/codesource.js ) et vous trouverez tous les exemples qui sont proposé dans le livre.

Si vous trouvez du code pas élégant ou non fonctionnel vous pouvez ouvrir une "Issue" et je m'empresserai de le corriger aussi tôt que possible!

A bientôt!
