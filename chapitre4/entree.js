/*
 *
 * Point d'entrée dans le programme.
 * */


var serveur = require("./serveur");
var requete = require("./processeurDeRequete");

var processeur = {};
processeur["/ecrire"] = requete.EcrireNouvelleTache;
processeur["/voir"] = requete.VoirTachesAFaire;
processeur["/supprimer"] = requete.SupprimerTache;

console.log("Entrée dans le serveur");
serveur.demarre(processeur);
console.log("Sortie du serveur");

