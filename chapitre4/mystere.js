var Emitter = require("events").EventEmitter;

var monEvent = new Emitter();

var arrete = false;

monEvent.on('stop', function () {
	arrete = true;
});

setTimeout(function () {
	monEvent.emit('stop');
}, 1000);


while (!arrete) {
	// On ne fait rien ici
}

console.log("Le programme a fini");
