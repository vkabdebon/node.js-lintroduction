/*
 * Démonstration de l'utilisation d'optimist!
 * */

var argv = require('optimist').argv;

if (argv.dit) {
	console.log("La console m'a dit: " + argv.dit);
} else {
	console.log("Rien n'est dit!");
}
