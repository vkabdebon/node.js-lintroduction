/*
 * bdd.js
 * Description: Contient les fonctions en rapport avec la base de données
 * Auteur: Victor Kabdebon
 * Site: http://www.victorkabdebon.com
 * */

var mongo = require("mongoskin");

function AjouterVisite(chemin) {
	var collection = mongo.db("localhost:27017/MonTODO", {'safe' : false}).collection("Visites"),
		nouveauDocument = {'Chemin': chemin, 'Date': new Date() };
	collection.insert(nouveauDocument, function (erreur) {
		console.log("Visite enregistrée");
	});
}


function CompterVisite(chemin) {
	var collection = mongo.db("localhost:27017/MonTODO", {'safe': false}).collection("Visites");
	collection.find({'Chemin': chemin}).count(function (erreur, compte) {
		console.log("Compte: " + compte);
	});

}

exports.AjouterVisite = AjouterVisite;
exports.CompterVisites = CompterVisite;
