var http = require("http");
var url = require("url");
var mongo = require("mongoskin");

http.createServer(function (request, response) {
	var collection = mongo.db("127.0.0.1:27017/MonTODO3", {'safe': false}).collection("Traqueur"),
		derniereVisite = url.parse(request.url).path;
	collection.insert({'Visite': derniereVisite}, function () {
		collection.find().toArray(function (error, elements) {
			for (var nombre in elements ) {
				response.write(elements[nombre].Visite);
				response.write("\n");
			}
			response.end();
		});
	});
}).listen(8888);
