/*
 * ProcesseurDeRequete.js
 * Description: -
 * Auteur: Victor Kabdebon
 * Site: http://www.victorkabdebon.com
 * */

var bdd = require("./bdd");

function EcrireNouvelleTache(requete, reponse) {
	reponse.write("Ecrire tache");	
	bdd.AjouterVisite("/ecrire");
	bdd.CompterVisites("/ecrire");
}

function VoirTachesAFaire(requete, reponse) {
	reponse.write("Voir taches"); 	
	bdd.AjouterVisite("/voir");
	bdd.CompterVisites("/voir");
}

function SupprimerTache(requete, reponse) {
	reponse.write("Supprimer tache");
	bdd.AjouterVisite("/supprimer");
	bdd.CompterVisites("/supprimer");
}

exports.EcrireNouvelleTache = EcrireNouvelleTache;
exports.VoirTachesAFaire = VoirTachesAFaire;
exports.SupprimerTache = SupprimerTache;
