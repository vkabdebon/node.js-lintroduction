/*
 * Point d'entrée dans le programme.
 * */


var serveur = require("./serveur");
var requete = require("./processeurDeRequete");
var bloquant = require("./bloquant");

var processeur = {};
processeur["/"] = bloquant.bloquant;
processeur["/ecrire"] = requete.EcrireNouvelleTache;
processeur["/voir"] = requete.VoirTachesAFaire;
processeur["/supprimer"] = requete.SupprimerTache;

console.log("Entrée dans le serveur");
serveur.demarre(processeur);
