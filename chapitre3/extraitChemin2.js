var http = require("http");
var url = require("url");

http.createServer(function (requete, reponse) {
	console.log("Connexion entrante!");
	var chemin = url.parse(requete.url);
	if (chemin.pathname === "/bonjour") {
		reponse.writeHead(200, {"Content-Type": "text/plain"});
		reponse.write("Bonjour Monde/Hello World");
	} else {
		reponse.writeHead(400, {"Content-Type": "text/plain"});
		reponse.write("ERREUR!");
	}
	console.log("Chemin demandé: " + chemin.pathname);
	reponse.end();
}).listen(8888);
console.log("Serveur démarré!");
