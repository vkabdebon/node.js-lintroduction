var http = require("http");
var url = require("url");

http.createServer(function (requete, reponse) {
	console.log("Connexion entrante!");
	reponse.writeHead(200, {"Content-Type": "text/plain"});
	reponse.write("Bonjour Monde/Hello World");
	var chemin = url.parse(requete.url);
	console.log("Chemin demandé: " + chemin.pathname);
	reponse.end();
}).listen(8888);
console.log("Serveur démarré!");
