/*
 * Processeur de requete
 * */

function EcrireNouvelleTache(requete, reponse) {
	reponse.write("Ecrire tache");
}

function VoirTachesAFaire(requete, reponse) {
	reponse.write("Voir taches");
}

function SupprimerTache(requete, reponse) {
	reponse.write("Supprimer tache");
}

exports.EcrireNouvelleTache = EcrireNouvelleTache;
exports.VoirTachesAFaire = VoirTachesAFaire;
exports.SupprimerTache = SupprimerTache;
