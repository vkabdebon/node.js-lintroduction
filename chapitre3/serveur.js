/*
 * Code pour le serveur
 * */

var http = require("http");
var url = require("url");

function demarre(processeurRequete) {
	var serveur = http.createServer(function (requete, reponse) {
		console.log("Requete");
		var req = url.parse(requete.url),
		    processeur = processeurRequete[req.path];
		if (typeof (processeur) === 'function') {
			processeur(requete, reponse);
			reponse.end();
		} else {
			console.log("Chemin non trouvé: " + req.path);
			reponse.write("Chemin non trouvé!");
			reponse.end();
		}
	});
	serveur.listen(8888);
}

exports.demarre = demarre;
