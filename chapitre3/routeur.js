/*
 * Routeur
 * */

function route() {
	console.log("Routeur initialisé pour l'adresse" + adresse);
	if ( typeof( processeur(adresse) === 'function' ) {
		return processeur(adresse)(reponse);
	} else {
		console.log("Aucune processeur de requête associé à l'adresse" + addresse);
		reponse.writeHead(404,{"Content-Head":"text/plain"} );
		response.write( "404 Not Found");
		response.end();
	}

}

exports.route = route;
