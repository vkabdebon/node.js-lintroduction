function bloquant(requete, reponse) {

	function superLongueFonction(i) {
		if (i === 1) {
			return 1;
		}
		if (i === 2) {
			return 1;
		}
		return superLongueFonction(i - 1) + superLongueFonction(i - 2);
	}
	reponse.write(String(superLongueFonction(45)));
}

exports.bloquant = bloquant;
