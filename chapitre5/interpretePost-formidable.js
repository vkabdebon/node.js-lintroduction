/*
 * interpretePost-formidable.js
 * Auteur: Victor Kabdebon
 * License: Mit
 * Site Web: http://www.victorkabdebon.com 
 * */

var http = require("http");
var formidable = require("formidable");

var page = '<!DOCTYPE html>' +
	'<html>' +
	'<head>' +
	'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
	'</head>' +
	'<body>' +
	'<form action="/" method="post">' +
	'<p>' +
	'<label for="prenom">Prénom: </label>' +
	'<input type="text" name="prenom"><br>' +
	'<label for="nomfamille">Nom de famille: </label>' +
	'<input type="text" name="nomfamille"><br>' +
	'<label for="email">Adresse Email: </label>' +
	'<input type="text" name="email"><br>' +
	'<input type="radio" name="sexe" value="Homme"> Homme<br>' +
	'<input type="radio" name="sexe" value="Femme"> Femme<br>' +
	'<input type="submit" value="Envoyer"> <input type="reset">' +
	'</p>' +
	'</form>' +
	'</body>' +
	'</html>';


function presenteFormulaire(requete, reponse) {
	reponse.writeHead(200, {"Content-Type": "text/html"});
	reponse.write(page);
	reponse.end();
}

function analyseFormulaire(champs) {
	console.log(champs);
}

http.createServer(function (requete, reponse) {
	if (requete.method === 'GET') {
		presenteFormulaire(requete, reponse);
	} else {
		var formulaire = new formidable.IncomingForm();
		formulaire.parse(requete, function (erreur, champs, fichiers) {
			analyseFormulaire(champs);
		});
		reponse.end();
	}
}).listen(8888);
