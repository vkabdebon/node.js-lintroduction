/*
 * interpretePost.js
 * Auteur: Victor Kabdebon
 * License: Mit
 * Site Web: http://www.victorkabdebon.com 
 * */

var http = require("http");

var page = '<!DOCTYPE html>' +
	"<html>" +
	'<head>' +
	'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' +
	'</head>' +
	'<body>' +
	'<form action="/" method="post">' +
	'<p>' +
	'<label> for="prenom">Prénom: </label>' +
	'<input type="text" name="prenom"><br>' +
	'<label for="nomfamille">Nom de famille: </label>' +
	'<input type="text" name="nomfamille"><br>' +
	'<label for="email">Adresse Email: </label>' +
	'<input type="text" name="email"><br>' +
	'<input type="radio" name="sexe" value="Homme"> Homme<br>' +
	'<input type="radio" name="sexe" value="Femme"> Femme<br>' +
	'<input type="submit" value="Envoyer"> <input type="reset">' +
	'</P>' +
	'</form>' +
	'</body>' +
	'</html>';


function presenteFormulaire(requete, reponse) {
	reponse.writeHead(200, {"Content-Type": "text/html"});
	reponse.write(page);
	reponse.end();
}

function analyseFormulaire(informationPost) {
	console.log(informationPost);
}

http.createServer(function (requete, reponse) {
	var donneesPost = "";
	if (requete.method === 'GET') {
		presenteFormulaire(requete, reponse);
	} else {
		requete.on("data", function (piecePost) {
			console.log("Reception de données");
			donneesPost += piecePost;
		});
		requete.on("end", function () {
			console.log("Fin de la réception des données");
			analyseFormulaire(donneesPost);
		});
		requete.addListener("close", function () {
			console.log("Fermeture de la connexion");
		});
		reponse.end();
	}
}).listen(8888);

