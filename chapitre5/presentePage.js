/*
 * presentePage.js
 * Auteur: Victor Kabdebon
 * License: Mit
 * Site Web: http://www.victorkabdebon.com 
 * */

var http = require("http");
var formidable = require("formidable");
var mu = require("mu2");

var cheminVersPages = "./pages/";

function analyseFormulaire(champs) {
	console.log(champs);
}

http.createServer(function (requete, reponse) {
	var stream, formulaire;
	if (requete.method === 'GET') {
		reponse.writeHead(200, {"Content-Type": "text/html"});
		stream = mu.compileAndRender(
			cheminVersPages + "formulaire.mustache",
			{'date': (new Date()).toLocaleTimeString() }
		);
		stream.pipe(reponse);
	} else {
		formulaire = new formidable.IncomingForm();
		formulaire.parse(requete, function (erreur, champs, fichiers) {
			analyseFormulaire(champs);
		});
		reponse.end();
	}
}).listen(8888);

